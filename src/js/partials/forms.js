;(function($) {
    /**
     * Default AjaxForm config
     * @type {{autoHelpBlock: boolean}}
     */
    var ajaxFormConfig = {
        autoHelpBlock: true
    };

    /**
     * Default Ajax Form Submited Handler
     * @param response
     */
    var ajaxFormSubmited = function(response) {
        if (response.status === 'success') {
			
            this.reset();
            sweetAlert("Выполнено", response.message, "success");
        } else {
            sweetAlert("Ошибка...", response.message, "error");
        }
    };

    
    /**
     * CallbackForm
     * @type {AjaxForm}
     */
    
	
	var leaveForm = new AjaxForm('#leave-form', ajaxFormConfig);
    leaveForm.onSubmited = ajaxFormSubmited;

    var clientForm = new AjaxForm('#client-form', ajaxFormConfig);
    clientForm.onSubmited = ajaxFormSubmited;
	
	
	
	$('[data-type="phone"]').mask("+7 (999) 999-99-99");
	

})(jQuery);