var ui = {
    wow:function () {
        wow = new WOW({
            boxClass: 'wow', // default
            animateClass: 'animated', // default
            offset: 300, // default
            mobile: false, // default
            live: true // default
        });
        wow.init();
    },
    scrollPage: function () {
        $(window).scroll(function() {
            if ($(window).scrollTop() >= $(".top-menu").height()) {
                $(".top-menu").addClass("colored");
                // $('#top').fadeIn();
            } else {
                $(".top-menu").removeClass("colored");
                // $('#top').fadeOut();
            }
        });
    },
    carousels: function () {
        // $('.smal-image-list').owlCarousel({
        //     nav : true,
        //     dots:true,
        //     items: 7,
        //     rtl: true,
        //     loop: true,
        //     margin: 15,
        //     center: true,
        //     mouseDrag: false,
        //     touchDrag: true
        // });
        $(window).on('load', function(){
            $('.smal-image-list .image').eq(0).addClass('active')
        })
        $('.smal-image-list').on('click', '.image', function(){
            // console.log($(this).css('background-image'))
            $('.smal-image-list .image').removeClass('active')
            var img = $(this).css('background-image');
            $('.big-product-image').css({
                'opacity': '0'
            });
            $(this).addClass('active')
            setTimeout(function(){
                $('.big-product-image').css({
                    'background-image': img,
                    'opacity': '1'
                });
            },100)
        })
    },
    product: function(){
        $('.count-but input[name="count-product"]').on('keyup', function(){
            if(Number($(this).val()) > 1 ){
                var price = $(this).parents().find('.total').attr('data-price');
                $('.total-block .total span').html(price * Number($('.count-but input[name="count-product"]').val()));
            }else{
                $(this).val(1);
                var price = $(this).parents().find('.total').attr('data-price');
                $('.total-block .total span').html(price * Number($('.count-but input[name="count-product"]').val()));
            }
        })
        $('.count-but').on('click', 'span', function(){
            if ($(this).hasClass('count-up')){
                $(this).parent().find('input').val(Number($(this).parent().find('input').val()) + 1);
                var price = $(this).parents('.prouct-but-block , .row').find('.total').attr('data-price');
                if (!price || price == ''){
                    $(this).parents().find('.total').attr('data-price', $(this).parents().find('.total').find('span').html());
                    price = $(this).parents().find('.total').children('span').html();
                }
                if ($(this).parents().hasClass('row')) {
                    $(this).parents('.row').find('.total').find('span').html(price * Number($(this).parents('.row').find('.count-but input[name="count-product"]').val()));
                    var priceTotal = 0;
                    var diskount = 0;
                    $('.basket-items .total span').each(function(){
                        priceTotal += Number($(this).html());
                    })
                    $('.basket-total .total-pr span').html(priceTotal)
                    if ($('.basket-total .total-discount span').html() || $('.basket-total .total-discount span').html() != ''){
                        $('.basket-total .total-all span').html(priceTotal - (priceTotal * Number($('.basket-total .total-discount span').html() / 100)))
                    }else{
                        $('.basket-total .total-all span').html(priceTotal)
                    }
                }else{
                    $(this).parents('.prouct-but-block').find('.total').find('span').html(price * Number($('.count-but input[name="count-product"]').val()));
                }
            }else{
                if ($(this).parent().find('input').val() >= 2){
                    $(this).parent().find('input').val(Number($(this).parent().find('input').val()) - 1);
                    var price = $(this).parents('.prouct-but-block , .row').find('.total').attr('data-price');
                    if (!price || price == ''){
                        $(this).parents().find('.total').attr('data-price', $(this).parents().find('.total').find('span').html());
                        price = $(this).parents().find('.total').find('span').html();
                    }
                    if ($(this).parents().hasClass('row')) {
                        $(this).parents('.row').find('.total').find('span').html(price * Number($(this).parents('.row').find('.count-but input[name="count-product"]').val()));
                        var priceTotal = 0;
                        var diskount = 0;
                        $('.basket-items .total span').each(function(){
                            priceTotal += Number($(this).html());
                        })
                        $('.basket-total .total-pr span').html(priceTotal)
                        if ($('.basket-total .total-discount span').html() || $('.basket-total .total-discount span').html() != ''){
                            $('.basket-total .total-all span').html(priceTotal - (priceTotal * Number($('.basket-total .total-discount span').html() / 100)))
                        }else{
                            $('.basket-total .total-all span').html(priceTotal)
                        }
                    }else{
                        $(this).parents('.prouct-but-block , .row').find('.total').find('span').html(price * Number($('.count-but input[name="count-product"]').val()));
                    }
                }
            }
            return false;

        })
    },
    modals: function(){
        $('.basket-but').on('click', function(){
            $('#modalBuy').fadeIn(300)
            return false;
        })
        $('.modal').on('click',function(e){
            var div = $(".modal .modal-content");
            if (!div.is(e.target)
                && div.has(e.target).length === 0) {
                $('.modal').fadeOut(300);
            }
        });
    },
    mobiles: function () {
        $('.mob-link .mob-menu-but').on('click', function(){
            $('body').toggleClass('mobile-menu')
        })
    },
    maps: function () {
        function mapInit () {
            var coordinates = [55.661408, 37.552175];
            var myMap = new ymaps.Map('contact-map', {
                center: coordinates,
                zoom: 10,
                controls: []
            });
            myMap.geoObjects.add(new ymaps.Placemark(coordinates, {
                }, {
                    iconImageSize: [40, 40],
                    iconImageOffset: [-40, 20]
                })
            )
        }
        if ($('#contact-map').length) {
            var heightMap = 400;
            $('#contact-map').css({
                'height':heightMap
            })
            ymaps.ready(mapInit);
        }
    },
    mainInit: function () {
        this.wow();
        this.scrollPage();
        this.carousels();
        this.product();
        this.modals();
        this.mobiles();
        this.maps();
    }
}

$(document).ready(function(){
    ui.mainInit();
    "use strict";
});
$(window).resize(function(){

})

