'use strict';

let gulp = require('gulp');
let watch = require('gulp-watch');
let prefixer = require('gulp-autoprefixer');
let uglify = require('gulp-uglify');
let sass = require('gulp-sass');
let sourcemaps = require('gulp-sourcemaps');
let rigger = require('gulp-rigger');
let cssmin = require('gulp-minify-css');
let imagemin = require('gulp-imagemin');
let pngquant = require('imagemin-pngquant');
let rimraf = require('rimraf');
let browserSync = require("browser-sync");
let notify = require("gulp-notify");
let livereload = require('gulp-livereload');
let reload = browserSync.reload;
let changed = require('gulp-changed');

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        css: 'build/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/script.js',//В стилях и скриптах нам понадобятся только main файлы
        style: 'src/style.scss',
        img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/style.scss',
        img: 'src/img/**/*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};
var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend_Devil"
};
gulp.task('html', function() {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(notify("Html compiled!"))
        .pipe(livereload())
        .pipe(reload({stream:true}));
});
gulp.task('js', () => {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        // .pipe(reload({stream: true})); //И перезагрузим сервер
        .pipe(livereload())
        .pipe(reload({stream:true}));
    gulp.src('src/js/vendors.js') //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(uglify()) //Сожмем наш js
        .pipe(gulp.dest('build/js/')) //Выплюнем готовый файл в build
        .pipe(livereload())
        .pipe(reload({stream:true}));
});
gulp.task('style', () => {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(notify("SCSS compiled!"))
        // .pipe(reload({stream: true}));
        .pipe(livereload())
        .pipe(reload({stream:true}));
    gulp.src('src/css/custom.scss') //Выберем наш main.scss
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css/')) //И в build
        .pipe(notify("SCSS compiled!"))
        .pipe(livereload())
        .pipe(reload({stream:true}));
});
gulp.task('image', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});
// gulp.task('fonts', function() {
//     gulp.src(path.src.fonts)
//         .pipe(gulp.dest(path.build.fonts))
// });
gulp.task('fonts', () => {
    gulp.src('src/fonts/**/*')
        .pipe(changed('build/fonts'))
        .pipe(gulp.dest('build/fonts'));

    // gulp.src('./src/vendor/**/*')
    //     .pipe(changed('./build/vendor'))
    //     .pipe(gulp.dest('./build/vendor'));
});
gulp.task('build', ['html','js','style','fonts','image']);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts');
    });
});
gulp.task('webserver', function () {
    browserSync(config);
});
// gulp.task('clean', function (cb) {
//     rimraf(path.clean, cb);
// });
gulp.task('default', ['build', 'webserver', 'watch']);